import { useState } from "react";
import "./App.css";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [textList, setTextList] = useState([]);

  return (
    <div className="App">
      <header className="App-header">
        <input
          onChange={(e) => {
            setInputValue(e.target.value);
          }}
        ></input>
        <button
          onClick={() => {
            setTextList([...textList, inputValue]);
          }}
        >
          Adicionar
        </button>
        <ul>
          {textList.map((text) => {
            return <li>{text}</li>;
          })}
        </ul>
      </header>
    </div>
  );
}

export default App;
